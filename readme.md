unity-git-hooks readme

This repository contains useful scripts for setting up a unity repository in git.

Please feel free to use these tools as they are provided under the MIT license.

This repo can be added as a submodule to your git repo:
```
$ git submodule add https://bitbucket.org/TeaClipper/tcg.hackspace.unity-git
```

Following the advice of Forrest Smith (https://blog.forrestthewoods.com/managing-meta-files-in-unity-713166ee3d30) the repo should be configured to handle Unity meta files as follows.

Directory meta files should be ignored from the repo:

```
$ ./tcg.hackspace.unity-git/scripts/ignore-dir-meta-files.sh
```

I’d then recommend going into git bash and removing any old folder meta files before installing the git hooks:
```
$ git rm – cached *.meta
$ git add *.meta
$ git commit -m “[-] removed folder meta files”
```

Install git hooks via git bash to check that assets and their corresponding meta files are kept in sync:
```
$ ./tcg.hackspace.unity-git/scripts/install-hooks.sh
```